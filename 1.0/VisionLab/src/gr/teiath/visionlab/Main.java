package gr.teiath.visionlab;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public class Main extends Application implements Initializable {
    @FXML
    Button startAnalysis,play,stop,clear;
    @FXML
    CheckMenuItem visScore;
    @FXML
    ImageView view;
    @FXML
    Label labelDrag,labelClick;
    @FXML
    ProgressIndicator progress;
    @FXML
    AreaChart<Number,Number> chart;
    @FXML
    Slider slidThres,slidWind,slidYsize,slidXsize;
    @FXML
    ComboBox choiceLog,choiceSub;
    @FXML
    TextField logPath;
    Stage stage;
    public static volatile VideoManager mVideoManager;
    public static volatile VideoAnalyzer mVideoAnalyzer;
    public long startTime;
    public String filePath, filename;
    public Mat previewframe=new Mat();
    public boolean areaSelected=false,areasFinalized=false,fileLoaded=false,analysisDataAvailable=false;
    public ArrayList<Rect> measAreas = new ArrayList<Rect>();
    public ArrayList<AreaChart.Series> analysisDataM = new ArrayList<AreaChart.Series>();
    public ArrayList<AreaChart.Series> analysisDataS = new ArrayList<AreaChart.Series>();
    public ArrayList<double[]> scoreData;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("layout.fxml"));
        primaryStage.setTitle("VisionLab 1.0");
        Scene scene = new Scene(root, 900,710);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(Main.class.getResource("styles.css").toExternalForm());
        primaryStage.show();
        stage = primaryStage;
    }

    @Override
    public void stop() {
        if (mVideoManager!=null){
            mVideoManager.stop();
        }
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources){
        logPath.setText(System.getProperty("user.home")+ File.separator+"Desktop"+File.separator+"VisionLab");
    }

    public void onStartAnalysis(ActionEvent actionEvent) {
        onStop(new ActionEvent());
        areasFinalized = true;
        startAnalysis.setDisable(true);
        labelClick.setVisible(false);
        play.setDisable(true);
        progress.setVisible(true);
        progress.setProgress(-1);
        mVideoAnalyzer = new VideoAnalyzer(filePath);
        mVideoAnalyzer.start(this,filename);
        view.setImage(null);
        view.setVisible(false);
        clear.setDisable(true);
    }

    public void onPlay(ActionEvent actionEvent) {
        areasFinalized = true;
        play.setDisable(true);
        mVideoManager = new VideoManager(filePath);
        mVideoManager.start(this);
        view.setVisible(true);
        stop.setDisable(false);
        startTime = System.currentTimeMillis();
        labelClick.setVisible(false);
        progress.setVisible(false);
        clear.setDisable(true);
    }

    public void onStop(ActionEvent actionEvent) {
        stop.setDisable(true);
        labelClick.setVisible(false);
        if(mVideoManager!=null)mVideoManager.stop();
        mVideoManager=null;
        if(mVideoAnalyzer!=null)mVideoAnalyzer.stop();
        mVideoAnalyzer=null;
        view.setImage(null);
        play.setDisable(false);
        startAnalysis.setDisable(false);
        loadpreview();
        view.setVisible(true);
        clear.setDisable(false);
        System.gc();
    }

    public void onMouseClick(MouseEvent mouseEvent){
        if(fileLoaded && !areasFinalized) {
            double x = (mouseEvent.getX() / view.getFitWidth());
            double y = mouseEvent.getY() / view.getFitHeight();
            double ratio = view.getFitWidth() / view.getFitHeight();
            Point startpoint=new Point((x+slidXsize.getValue()*0.01)*previewframe.width(),(y+slidYsize.getValue()*0.01*ratio)*previewframe.height());
            Point endpoint=new Point((x-slidXsize.getValue()*0.01)*previewframe.width(),(y-slidYsize.getValue()*0.01*ratio)*previewframe.height());
            measAreas.add(new Rect(startpoint,endpoint));
            areaSelected=true;
            showpreview();
            startAnalysis.setDisable(false);
            play.setDisable(false);
            clear.setDisable(false);
            choiceLog.setDisable(false);
            choiceSub.setDisable(false);
            if(measAreas.size()>1) choiceSub.getItems().add(Integer.toString(measAreas.size()-1));
        }
    }

    public void onFileEnter(DragEvent event) {
        if(!fileLoaded) {
            Dragboard db = event.getDragboard();
            if (db.hasFiles()) {
                event.acceptTransferModes(TransferMode.COPY);
            } else {
                event.consume();
            }
        }
    }

    public void onFileDrop(DragEvent event){
        if(!fileLoaded) {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                success = true;
                for (File file : db.getFiles()) {
                    filePath = file.getAbsolutePath();
                }
                openFile();
            }
            event.setDropCompleted(success);
            event.consume();
        }
    }

    public void onFileOpen(ActionEvent event){
        if(!fileLoaded) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Experiment Video");
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                filePath = file.getPath();
                openFile();
            }
        }
    }

    public void openFile(){
        labelDrag.setVisible(false);
        labelClick.setVisible(true);
        fileLoaded=true;
        File f = new File(filePath);
        filename=f.getName().substring(0,f.getName().lastIndexOf('.'));
        loadpreview();
    }

    public void onFileClose(ActionEvent actionEvent) {
        if(fileLoaded) {
            analysisDataAvailable=false;
            analysisDataM.clear();
            analysisDataS.clear();
            fileLoaded = false;
            areaSelected = false;
            areasFinalized = false;
            measAreas.clear();
            chart.getData().clear();
            if (mVideoManager != null) mVideoManager.stop();
            mVideoManager=null;
            if(mVideoAnalyzer!=null)mVideoAnalyzer.stop();
            mVideoAnalyzer= null;
            view.setImage(null);
            view.setVisible(false);
            progress.setVisible(false);
            startAnalysis.setDisable(true);
            stop.setDisable(true);
            play.setDisable(true);
            labelDrag.setVisible(true);
            labelClick.setVisible(false);
            System.gc();
        }
    }

    public void onClearSubjects(ActionEvent actionEvent){
        measAreas.clear();
        loadpreview();
        chart.getData().clear();
        areaSelected= false;
        areasFinalized = false;
        progress.setVisible(false);
        labelClick.setVisible(true);
        showpreview();
        startAnalysis.setDisable(true);
        play.setDisable(true);
        clear.setDisable(true);
        choiceLog.setDisable(true);
        choiceSub.setDisable(true);
    }

    public void onPlotDataChanged(ActionEvent actionEvent) {
        plotData(choiceSub.getValue().toString());
    }

    public void plotData(String sub) {
        if (analysisDataAvailable){
            chart.getData().clear();
            chart.getData().add(analysisDataM.get(Integer.parseInt(sub)));
            chart.getData().add(analysisDataS.get(Integer.parseInt(sub)));
        }
    }

    public void loadpreview(){
        VideoCapture prevcap=new VideoCapture(filePath);
        for (int i = 0; i < 10 && prevcap.isOpened();i++){
            prevcap.read(previewframe);
        }
        if(previewframe.size().area()>500000) Imgproc.pyrDown(previewframe, previewframe);//High res only
        prevcap.release();
        view.setVisible(true);
        showpreview();
    }

    public void showpreview(){
        for(int i=0; i<measAreas.size();i++) {
            Imgproc.rectangle(previewframe, measAreas.get(i).tl(), measAreas.get(i).br(), new Scalar(255, 50, 0), 3);
            Imgproc.putText(previewframe,Integer.toString(i),measAreas.get(i).br(),Core.FONT_HERSHEY_SIMPLEX,1,new Scalar(255, 50, 0));
        }
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".jpg", previewframe, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        Image prev;
        try {
            BufferedImage img = ImageIO.read(in);
            prev = SwingFXUtils.toFXImage(img, null);
            onImage(prev);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onImage(Image image){
        view.setImage(image);
    }

    public static void main(String[] args) {
        System.loadLibrary("opencv_java300");
        System.loadLibrary("opencv_ffmpeg300_64");
        launch(args);
    }
}
