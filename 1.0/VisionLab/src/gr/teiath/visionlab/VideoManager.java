package gr.teiath.visionlab;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class VideoManager {
    public volatile VideoCapture Capture;
    Image image;
    boolean endvideo=false;
    double framerate,framecount,duration;
    Main UI;
    Thread CameraManager=new Thread(new Runnable() {
        @Override
        public void run() {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            //Initialization
            Mat frame=new Mat();
            int framepos=0;

            while (Capture.isOpened()&&!endvideo) {
                Mat image = new Mat();
                if(!Capture.isOpened())break;
                Capture.read(image);

                if(!image.empty()&& UI!=null) {
                    if(image.size().area()>500000) Imgproc.pyrDown(image,image);//High res only
                    long startproc=System.currentTimeMillis();
                    image.copyTo(frame);

                    //Processing
                    for(int i=0; i<UI.measAreas.size();i++) {
                        Imgproc.rectangle(frame, UI.measAreas.get(i).tl(), UI.measAreas.get(i).br(), new Scalar(255, 50, 0), 3);
                        Imgproc.putText(frame,Integer.toString(i),UI.measAreas.get(i).br(),Core.FONT_HERSHEY_SIMPLEX,1,new Scalar(255, 50, 0));

                        if(UI.analysisDataAvailable&&UI.visScore.isSelected()){
                            if(UI.scoreData.get(i)[framepos]>0){
                                Imgproc.rectangle(frame, UI.measAreas.get(i).tl(), UI.measAreas.get(i).br(), new Scalar(0, 200, 0), 3);
                                Imgproc.putText(frame,Integer.toString(i),UI.measAreas.get(i).br(),Core.FONT_HERSHEY_SIMPLEX,1,new Scalar(0, 200, 0));
                            }
                        }

                    }
                    framepos++;
                    //Show an image
                    show(frame);

                    long proctime=System.currentTimeMillis()-startproc;
                    long delay=Math.round(1000 / framerate)-proctime;
                    if(delay<0)delay=0;
                    cvWait(delay);
                    //System.gc();
                }
                else break;
            }
            System.gc();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    UI.clear.setDisable(false);
                }
            });
            if(!endvideo) Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    UI.onStop(new ActionEvent());
                }
            });
        }
        public void cvWait(long time){
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        public void show(Mat frame){
            Mat showframe=new Mat();
            frame.copyTo(showframe);

            MatOfByte bytemat = new MatOfByte();
            Imgcodecs.imencode(".jpg", showframe, bytemat);
            byte[] bytes = bytemat.toArray();
            InputStream in = new ByteArrayInputStream(bytes);
            try {
                BufferedImage img = ImageIO.read(in);
                image = SwingFXUtils.toFXImage(img, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try{UI.onImage(image);}
                    catch (NullPointerException e){}
                }
            });
        }
    });

    public VideoManager(String file){
        Capture= new VideoCapture(file);
        framerate =  Capture.get(Videoio.CAP_PROP_FPS);
        framecount = Capture.get(Videoio.CAP_PROP_FRAME_COUNT);
        duration = framecount/framerate;

        //System.out.println("Framerate: " + framerate);
        //System.out.println("Frames: " + framecount);
        //System.out.println("Duration: " + duration/60);

    }

    public VideoManager(Integer cam){
        Capture= new VideoCapture(cam);
        framerate=24;
    }

    public void start(Main main) {
        CameraManager.start();
        UI=main;
    }

    public void stop() {
        endvideo=true;
        UI=null;
        try {Thread.sleep(50);}
        catch (InterruptedException e){e.printStackTrace();}
        UI=null;
        Capture.release();
    }

}
