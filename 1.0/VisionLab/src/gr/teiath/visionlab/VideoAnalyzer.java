package gr.teiath.visionlab;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.chart.AreaChart;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class VideoAnalyzer {
    public volatile VideoCapture Capture;
    double curprogress;
    boolean endvideo=false;
    double framerate,framecount,duration;
    Main UI;
    String filename;
    BufferedWriter logger=null;
    Thread CameraManager=new Thread(new Runnable() {
        @Override
        public void run() {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            //Initialization
            double[] time = new double[(int)framecount];
            ArrayList<double[]> vecPower = new ArrayList<double[]>();
            for(int i=0; i<UI.measAreas.size();i++) vecPower.add(i,new double[(int)framecount]);
            ArrayList<double[]> vecNum = new ArrayList<double[]>();
            for(int i=0; i<UI.measAreas.size();i++) vecNum.add(i,new double[(int)framecount]);
            Mat frame = new Mat();
            Mat prevframe = Mat.zeros((int)Capture.get(Videoio.CAP_PROP_FRAME_HEIGHT), (int)Capture.get(Videoio.CAP_PROP_FRAME_WIDTH),CvType.CV_8UC3);

            while (Capture.isOpened()&&!endvideo) {
                Mat image = new Mat();
                if(!Capture.isOpened())break;
                Capture.read(image);

                if(!image.empty()) {
                    image.copyTo(frame);

                    int idx = (int)Capture.get(Videoio.CAP_PROP_POS_FRAMES)-1;
                    time[idx] = Capture.get(Videoio.CAP_PROP_POS_MSEC);

                    //Processing
                    for(int i=0; i<UI.measAreas.size();i++) {
                        Mat measurementArea=frame.submat(UI.measAreas.get(i));
                        Mat prevMeasurementArea=prevframe.submat(UI.measAreas.get(i));
                        Imgproc.cvtColor(measurementArea,measurementArea,Imgproc.COLOR_BGR2GRAY);
                        Imgproc.cvtColor(prevMeasurementArea, prevMeasurementArea, Imgproc.COLOR_BGR2GRAY);

                        MatOfPoint flow = new MatOfPoint();
                        Mat graysmall = new Mat();
                        Mat prevsmall = new Mat();
                        Imgproc.resize(measurementArea,graysmall,new Size(300,300));
                        Imgproc.resize(prevMeasurementArea,prevsmall,new Size(300,300));
                        Double motion = 0.0;
                        Integer points = 0;
                        Video.calcOpticalFlowFarneback(prevsmall, graysmall, flow, 0.5, 3, 15, 3, 5, 1.2, 0);
                        for (int y = 0; y < flow.rows(); y += 6){
                            for (int x = 0; x < flow.cols(); x += 6) {
                                double[] data= flow.get(y,x);
                                Point pt = new Point(data[0],data[1]);
                                if(  (Math.abs(((double) pt.y)/((double)pt.x))) > Math.tan(40*Math.PI/180)  &&   Math.abs(pt.y) > 3) {
                                    motion+=Math.sqrt(Math.pow(pt.x,2)+Math.pow(pt.x,2));
                                    points++;
                                }
                            }
                        }
                        vecPower.get(i)[idx] = motion;
                        vecNum.get(i)[idx] = points;
                    }
                    image.copyTo(prevframe);

                    setProgress(time[idx]/(duration*1000));
                    System.gc();
                }
                else break;
            }
            setProgress(-1);

            //Process Output Data
            int window = (int) UI.slidWind.getValue();
            double threshold = UI.slidThres.getValue();
            int n = (int)framecount;
            ArrayList<double[]> movMax = new ArrayList<double[]>();
            ArrayList<double[]> behavData = new ArrayList<double[]>();
            for(int i=0; i<UI.measAreas.size();i++){
                movMax.add(i,new double[(int)framecount]);
                behavData.add(i,new double[(int)framecount]);
            }
            ArrayList<Double> immobilityData = new ArrayList<Double>();
            for(int id=0; id<UI.measAreas.size();id++) {
                DescriptiveStatistics stats = new DescriptiveStatistics();
                stats.setWindowSize(window);
                for(int i = 0; i<n; i++ ){
                    stats.addValue(vecNum.get(id)[i]);
                    movMax.get(id)[i] = stats.getMax();
                }

                for(int i = 0; i<n; i++ ) if (movMax.get(id)[i] >= threshold) behavData.get(id)[i] = 1;
                int zeros=0;
                for(int i = 0; i<n; i++ ) if (behavData.get(id)[i] == 0) zeros++;
                immobilityData.add(id,zeros/framerate);
            }

            //Show Output Data
            UI.analysisDataM.clear();
            UI.analysisDataS.clear();
            for(int id=0; id<UI.measAreas.size();id++) {
                UI.analysisDataM.add(id,new AreaChart.Series<Number,Number>());
                UI.analysisDataM.get(id).setName("Mobility");
                UI.analysisDataM.get(id).getData().add(new AreaChart.Data<Number, Number>(time[0], 0));
                for(int i = 1; i<n; i+=10 ) UI.analysisDataM.get(id).getData().add(new AreaChart.Data<Number, Number>(time[i], vecNum.get(id)[i]));
                UI.analysisDataS.add(id,new AreaChart.Series<Number,Number>());
                UI.analysisDataS.get(id).setName("Score");
                for(int i = 0; i<n; i+=10 ) UI.analysisDataS.get(id).getData().add(new AreaChart.Data<Number, Number>(time[i], threshold*behavData.get(id)[i]));
            }
            UI.analysisDataAvailable=true;

            try{
                Thread.sleep(100);
            }catch (InterruptedException e){}
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    UI.onPlotDataChanged(new ActionEvent());
                }
            });

            //Log Output Data
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
            String logfilename = filename + "_" + sdf.format(new Date()) + ".txt";
            String path = UI.logPath.getText();
            File f = new File(path);
            f.mkdirs();
            try{
                logger = new BufferedWriter(new FileWriter(new File(path,logfilename)));
            }
            catch(Exception e){e.printStackTrace();}
            if(UI.choiceLog.getValue().equals("Stats")){
                try{
                    logger.append("Total Immobility (s)");
                    for(int i=0; i<UI.measAreas.size();i++) {
                        logger.append(String.format("\t%.2f",immobilityData.get(i)));
                    }
                    logger.newLine();
                    logger.append("Total Mobility (s)");
                    for(int i=0; i<UI.measAreas.size();i++) {
                        logger.append(String.format("\t%.2f",duration-immobilityData.get(i)));
                    }
                    logger.newLine();
                    logger.append(String.format("Area Width\t%.2f",UI.slidXsize.getValue()));
                    logger.newLine();
                    logger.append(String.format("Area Height\t%.2f",UI.slidYsize.getValue()));
                    logger.newLine();
                    logger.append(String.format("Window\t%d",window));
                    logger.newLine();
                    logger.append(String.format("Threshold\t%.1f",threshold));
                    logger.newLine();
                }
                catch (Exception e){}
            }
            else if(UI.choiceLog.getValue().equals("Timeline")){
                for(int ind=0 ; ind<time.length ; ind++){
                    try{
                        logger.append(String.format("%.2f",time[ind]));
                        for(int i=0; i<UI.measAreas.size();i++) {
                            logger.append(String.format("\t%.0f",behavData.get(i)[ind]));
                            logger.append(String.format("\t%.0f",vecNum.get(i)[ind]));
                            logger.append(String.format("\t%.2f",vecPower.get(i)[ind]));
                        }
                        logger.newLine();
                    }
                    catch (Exception e){}
                }
            }
            try{
                logger.close();
            }
            catch(Exception e){}

            UI.scoreData=behavData;
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    UI.clear.setDisable(false);
                }
            });
            setProgress(1);
            System.gc();
            if(!endvideo) Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    UI.onStop(new ActionEvent());
                }
            });
        }
        public void setProgress(double prog){
            curprogress=prog;
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        UI.progress.setProgress(curprogress);
                    } catch (Exception e) {
                    }
                }
            });
        }
    });

    public VideoAnalyzer(String file){
        Capture= new VideoCapture(file);
        framerate =  Capture.get(Videoio.CAP_PROP_FPS);
        framecount = Capture.get(Videoio.CAP_PROP_FRAME_COUNT);
        duration = (framecount/framerate);

        //System.out.println("Framerate: " + framerate);
        //System.out.println("Frames: " + framecount);
        //System.out.println("Duration: " + duration+"ms");

    }
    public VideoAnalyzer(Integer cam){
        Capture= new VideoCapture(cam);
        framerate=24;
    }

    public void start(Main main,String filename) {
        this.filename=filename;

        CameraManager.start();
        UI=main;
    }

    public void stop() {
        endvideo=true;
        try {Thread.sleep(50);}
        catch (InterruptedException e){e.printStackTrace();}
        UI=null;
        Capture.release();
        try{
            logger.close();
        }
        catch(Exception e){}
    }

}
